<?php

header('Content-Type: text/html; charset=UTF-8');

$ability_labels = ['god' => 'Godless', 'idclip' => 'Indoorless', 'Levitation' => 'Levitation'];
$ability_data = array_keys($ability_labels);
$user = 'u16364';
$pass = '6346523';
$save = '<script>
alert("Спасибо, Ваш запрос сохранён!");
</script>';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    print($save);
  }

  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['Wfio'] = !empty($_COOKIE['Wfio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['Wemail'] = !empty($_COOKIE['Wemail_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['Wdate'] = !empty($_COOKIE['Wdate_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['some'] = !empty($_COOKIE['some_error']);
  $errors['about'] = !empty($_COOKIE['about_error']);
  $errors['Wabout'] = !empty($_COOKIE['Wabout_error']);
  $errors['accept'] = !empty($_COOKIE['accept_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);

  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    $messages_fio = '<div class="error">Заполните имя.</div>';
  }

  if ($errors['Wfio']) {
    setcookie('Wfio_error', '', 100000);
    $messages_fio = '<div class="error">Введите русские буквы!</div>';
  }

  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages_email = '<div class="error">Заполните email.</div>';
  }

  if ($errors['Wemail']) {
    setcookie('Wemail_error', '', 100000);
    $messages_email = '<div class="error">Введите корректно email. Ex: abc@me.ru.</div>';
  }

  if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages_date = '<div class="error">Заполните дату.</div>';
  }

  if ($errors['Wdate']) {
    setcookie('Wdate_error', '', 100000);
    $messages_date = '<div class="error">Заполните дату в формате: YYYY-MM-DD</div>';
  }

  if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages_sex = '<div class="error">Заполните пол.</div>';
  }

  if ($errors['some']) {
    setcookie('some_error', '', 100000);
    $messages_some = '<div class="error">Выберите конечность.</div>';
  }

  if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages_abilities = '<div class="error">Выберите сверхспособность.</div>';
  }

  if ($errors['about']) {
    setcookie('about_error', '', 100000);
    $messages_about = '<div class="error">Заполните информацию о себе.</div>';
  }

  if ($errors['Wabout']) {
    setcookie('Wabout_error', '', 100000);
    $messages_about = '<div class="error">Заполните информацию о себе по-русски.</div>';
  }

  if ($errors['accept']) {
    setcookie('accept_error', '', 100000);
    $messages_accept = '<div class="error">Поставьте галочку, пожалуйста.</div>';
  }

  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['some'] = empty($_COOKIE['some_value']) ? '' : $_COOKIE['some_value'];
  $values['about'] = empty($_COOKIE['about_value']) ? '' : $_COOKIE['about_value'];
  $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];

  include('form.php');
} else {
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
    setcookie('Wfio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60 * 12);
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/(^[\w.]+)@([\w]+)\.([a-zA-Z]+$)/u', $_POST['email'])) {
    setcookie('Wemail_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60 * 12);
  }

  if (empty($_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)/', $_POST['date'])) {
    setcookie('Wdate_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60 * 12);
  }

  if (empty($_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60 * 12);
  }

  if (empty($_POST['some'])) {
    setcookie('some_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('some_value', $_POST['some'], time() + 30 * 24 * 60 * 60 * 12);
  }


  if (empty($_POST['abilities'])) {
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    $abilities = $_POST['abilities'];
    setcookie('abilities_value', implode(($_POST['abilities'])), time() + 30 * 24 * 60 * 60 * 12);
  }


  if (empty($_POST['about'])) {
    setcookie('about_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if (!preg_match('/^[а-яА-Я ].+$/u', $_POST['about'])) {
    setcookie('Wabout_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('about_value', $_POST['about'], time() + 30 * 24 * 60 * 60 * 12);
  }

  if (empty($_POST['accept'])) {
    setcookie('accept_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    setcookie('accept_value', $_POST['accept'], time() + 30 * 24 * 60 * 60 * 12);
  }


  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  } else {
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('some_error', '', 100000);
    setcookie('about_error', '', 100000);
    setcookie('accept_error', '', 100000);
    setcookie('abilities_error', '', 100000);
  }

  $ability_insert = [];
  foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? '+' : '-';
  }

  $db = new PDO(
    'mysql:host=localhost;dbname=u16364',
    $user,
    $pass,
    array(PDO::ATTR_PERSISTENT => true)
  );

  try {
    $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, date = ?, sex = ?, amount_of_legs = ?, ability_god = ?, ability_indoor = ?, ability_levitation = ?, about = ? ");
    $stmt->execute(array($_POST['fio'], $_POST['email'], $_POST['date'], $_POST['sex'], $_POST['some'], $ability_insert['god'], $ability_insert['idclip'], $ability_insert['Levitation'], $_POST['about']));
  } catch (PDOException $err) {
    print('Error : ' . $err->getMessage());
    exit();
  }
  setcookie('save', '1');
  header('Location: index.php');
}
